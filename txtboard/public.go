package txtboard

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	log "github.com/rs/zerolog"
)

type Server struct {
	DB     *gorm.DB
	Logger log.Logger
}

func NewServer(dbPath string, logger log.Logger) Server {
	db, err := gorm.Open("sqlite3", dbPath)
	if err != nil {
		panic("failed to connect database")
	}

	db.AutoMigrate(&Post{})

	serv := &Server{
		DB:     db,
		Logger: logger,
	}

	return *serv
}

type Post struct {
	ID       uint `gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	ParentID uint
	Deep     byte
	Content  string
}

func (s Server) CreatePost(parentID uint, deepness byte, content string) {
	var postType string

	post := Post{ParentID: parentID,
		Deep:    deepness,
		Content: content,
	}

	s.DB.NewRecord(post)
	s.DB.Create(&post)

	switch deepness {
	case 0:
		postType = "Root"
		break
	case 1:
		postType = "Board"
		break
	case 2:
		postType = "Thread"
		break
	case 3:
		postType = "Message"
		break
	default:
		s.Logger.
			Warn().Uint("ParentID", parentID).Str("Content", content).
			Msg(fmt.Sprintf("Created post with deepness %d", deepness))
		break
	}

	s.Logger.
		Info().Uint("ParentID", parentID).Str("Content", content).
		Msg(fmt.Sprintf("Created %s", postType))
}

func (s Server) Serve(parentID uint) {
	s.Logger.Info().Msg("Serving in text mode")

	fmt.Println(s.formatForTTY(0, 0))
}

// format recursively and space with ' |'
func (s Server) formatForTTY(parentID uint, deep int) string {
	var ps []Post
	spacer := strings.Repeat(" |", deep)

	s.DB.Where("parent_id = ?", parentID).Find(&ps)

	r := ""

	for _, p := range ps {
		// Generate post presentation
		r += fmt.Sprintf("%s >>>%d\n%s-> %s\n",
			spacer,
			p.ID,
			spacer,
			p.Content)
		// Append all children posts with deep += 1
		r += s.formatForTTY(p.ID, deep+1)
	}

	return r
}
