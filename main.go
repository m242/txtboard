package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"m242/txtboard/txtboard"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
)

var serv txtboard.Server

func main() {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	serv = txtboard.NewServer("/tmp/txtboard.db", logger)

	// Fake Board
	serv.CreatePost(0, 0, "What a nice textboard") // ID 1

	// Serve from root
	// Mux router
	r := mux.NewRouter()
	// Routes
	r.HandleFunc("/", rootHandler).Methods("GET")
	r.HandleFunc("/{ID}", postReadingHandler).Methods("GET")
	r.HandleFunc("/{ID}", postCreatingHandler).Methods("POST")

	srv := &http.Server{
		Addr: "0.0.0.0:8080",
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			serv.Logger.Error().Msg(err.Error())
		}
	}()

	// Block while server is alive
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)
	// Block until we receive our signal.
	<-c

	// Cleanup
	serv.DB.Close()
}

func postReadingHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, err := strconv.ParseUint(vars["ID"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	}

	fmt.Fprint(w, htmlFormatServerPost((uint)(postID)))
}

func postCreatingHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	parentID, err := strconv.ParseUint(vars["ID"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	}

	if err := r.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	content := r.FormValue("content")

	// Deepness
	var parent txtboard.Post
	serv.DB.Where("id =?", parentID).First(&parent)

	deepness := parent.Deep + 1

	serv.CreatePost((uint)(parentID), deepness, content)

	http.Redirect(w, r, fmt.Sprintf("/%d", parentID), http.StatusFound)
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, htmlFormatServerPost(1))
}

func htmlFormatServerPost(id uint) string {
	var post txtboard.Post
	var children []txtboard.Post
	r := "<body>"

	serv.DB.Where("id = ?", id).First(&post)
	r += fmt.Sprintf("<a href='/%d'>>%d</a> | %s<br>",
		id,
		id,
		post.Content)

	spacer := "|-->"
	serv.DB.Where("parent_id = ?", id).Find(&children)
	for _, p := range children {
		// Generate post presentation
		r += fmt.Sprintf("%s <a href='/%d'>>>%d</a> | %s<br>",
			spacer,
			p.ID,
			p.ID,
			p.Content)
	}

	if post.Deep < 3 {
		r += fmt.Sprintf(
			"<form action='/%d' method='post'><label for='content'>New:</label><input type='text' name='content'><br><input type='submit' value='Submit' data-rel='back'> </form>",
			post.ID)
	}
	r += "</body>"
	return r
}
