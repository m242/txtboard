module m242/txtboard

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.16
	github.com/rs/zerolog v1.19.0
)
